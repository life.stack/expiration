import Queue from 'bull'
import { natsWrapper } from '../natsWrapper'
import { Subjects } from '@life.stack/common'
import { GroupInvitationExpiredPublisher } from '../events/publishers/groupInvitationExpiredPublisher'

interface Payload {
  invitationId: string
}

const groupInvitationExpirationQueue = new Queue<Payload>(
  'group:invite:expiration',
  {
    redis: {
      host: process.env.REDIS_HOST,
    },
  }
)

groupInvitationExpirationQueue.process(async (job) => {
  console.log(
    `${Subjects.GroupInvitationExpired.toString()} for invitationId:`,
    job.data.invitationId
  )
  await new GroupInvitationExpiredPublisher(natsWrapper.client).publish({
    invitationId: job.data.invitationId,
  })
})

export { groupInvitationExpirationQueue }
