import {
  ExpirationCompleteEvent,
  Publisher,
  Subjects,
} from '@life.stack/common'

export class ExpirationCompletePublisher extends Publisher<ExpirationCompleteEvent> {
  subject: Subjects.ExpirationComplete = Subjects.ExpirationComplete

  publish(data: ExpirationCompleteEvent['data']): Promise<void> {
    return super.publish(data)
  }
}
