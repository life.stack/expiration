import {
  GroupInvitationCreatedEvent,
  Listener,
  Subjects,
} from '@life.stack/common'
import { queueGroupName } from './queueGroupName'
import { Message } from 'node-nats-streaming'
import { groupInvitationExpirationQueue } from '../../queues/groupInvitationExpirationQueue'

export class GroupInvitationCreatedListener extends Listener<GroupInvitationCreatedEvent> {
  subject: Subjects.GroupInvitationCreated = Subjects.GroupInvitationCreated
  queueGroupName = queueGroupName

  async onMessage(data: GroupInvitationCreatedEvent['data'], msg: Message) {
    const delay = new Date(data.expiresAt).getTime() - new Date().getTime()
    console.log(`Expiring Invitation ${data.id} in ${delay}ms`)

    await groupInvitationExpirationQueue.add(
      {
        invitationId: data.id,
      },
      { delay }
    )

    msg.ack()
  }
}
