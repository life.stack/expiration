import { natsWrapper } from '../../../natsWrapper'
import { GroupInvitationCreatedEvent } from '@life.stack/common'
import { GroupInvitationCreatedListener } from '../groupInvitationCreatedListener'
import { groupInvitationExpirationQueue } from '../../../queues/groupInvitationExpirationQueue'

const setup = async () => {
  // Create an instance of the listener
  const listener = new GroupInvitationCreatedListener(natsWrapper.client)
  // Create a fake data event
  const data: GroupInvitationCreatedEvent['data'] = {
    id: 'mongoose.Types.ObjectId().toHexString()',
    expiresAt: new Date().toISOString(),
  }

  // create a fake message object
  // @ts-ignore
  const msg: Message = {
    ack: jest.fn(),
  }

  return { listener, data, msg }
}

it('adds a message to the expiration queue', async () => {
  const { listener, data, msg } = await setup()

  await listener.onMessage(data, msg)

  expect(groupInvitationExpirationQueue.add).toHaveBeenCalled()
})

it('acks the message', async () => {
  const { listener, data, msg } = await setup()

  await listener.onMessage(data, msg)

  expect(msg.ack).toHaveBeenCalled()
})
